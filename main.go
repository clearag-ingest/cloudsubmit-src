/*
Get RMQ message from pusher queue
Open file & upload to S3
**ACK/NACK message
*/
package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"cloudingest/datapusher"
	"cloudingest/rmqutils"

	"github.com/google/uuid"
	"github.com/kkyr/fig"
	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

const (
	EU_RMQ_HOST       = "internal-rabbitmq-c1-169997419.eu-west-1.elb.amazonaws.com"
	US_RMQ_HOST       = "internal-rabbitmq-c1-525988448.us-east-1.elb.amazonaws.com"
	DEV_RMQ_HOST      = "internal-rabbitmq-c1-940220365.us-east-1.elb.amazonaws.com"
	DEV_BUCKET        = "wxgrid-dev.clearag.com"
	PROD_BUCKET       = "grid-data.clearag.com"
	ARCHIVE_BUCKET    = "cag-grid-data-archive-us-east-1"
	SUBMITTING_SUFFIX = "cloudsubmit"
)

// returns the expected absolute app root for cloudsubmit
func getAppRoot() string {
	p, err := os.Executable()
	if err != nil {
		panic(err)
	}
	appRoot, _ := filepath.Split(filepath.Dir(p))
	return appRoot
}

//goland:noinspection GoUnusedGlobalVariable
var (
	//uri          = flag.String("uri", "amqp://wxgrid:wxgrid@andal-dev-00.meridian-enviro.com/wxgrid", "AMQP URI")
	//exchange     = flag.String("exchange", "wxgrid.ha.notifier_header_exchange", "Durable, non-auto-deleted AMQP exchange name")
	//exchangeType = flag.String("exchange-type", "headers", "Exchange type - direct|fanout|topic|x-custom")
	//queue        = flag.String("queue", "wxgrid.ha.ec2.notifier_pusher_queue", "Ephemeral AMQP queue name")
	//bindingKey   = flag.String("key", "wxgrid.ha.ec2.notifier_pusher_queue", "AMQP binding key")
	workers = flag.Int("workers", 30, "number of concurrent workers that are uploading files to s3")
	// AWS
	S3Bucket  = flag.String("bucket", "", "S3 Bucket name where gridded data is put. Data put in the s3 bucket will use the bucket's lifecycle rules. Setting this will override the default bucket when using the -submit_dc argument.")
	S3Timeout = flag.Duration("s3Timeout", 30*time.Second, "S3 upload timeout.")
	S3Region  = flag.String("s3Region", "us-east-1", "S3 Region")
	S3Profile = flag.String("s3_profile", "", "Target AWS role to use from configuration")
	// RabbitMq
	rmqHost            = flag.String("rmq_host", "internal-rabbitmq-c1-940220365.us-east-1.elb.amazonaws.com", "single rabbitMQ host")
	rmqPort            = flag.Int("rmq_port", 5672, "rabbitMQ port, default should be fine in most cases")
	rmqUser            = flag.String("rmq_user", "wxgrid", "Rabbitmq username")
	rmqPassword        = flag.String("rmq_password", "wxgrid", "Rabbitmq password")
	rmqVhost           = flag.String("rmq_vhost", "wxgrid", "Rabbitmq virtual host")
	defaultRmqExchange = "wxgrid.ha.notifier_header_exchange" // will be clobbered by env var RABBITMQ_NOTIFIER_HEADER_EXCHANGE
	rmqExchange        = flag.String("rmq_exchange", "wxgrid.ha.notifier_header_exchange", "Rabbitmq exchange")
	//rmqBaseQueueName   = flag.String("rmq_queue_base", "internal-rabbitmq-c1-940220365.us-east-1.elb.amazonaws.com", "used to setup/ send messages to. The base name will have .<region> appended")
	// Submission Configuration
	region            = flag.String("region", "ALL", "region that we want to send messages to (eg, US or EU). Defaults to ALL")
	defaultPath       = flag.String("base-path", "", "prepend to blob name from notifier event")
	notifierFile      = flag.String("notifier-from-file", "/notifier_file", "path to notifier file")
	notifierEvent     = flag.String("notifier_event", "", "single event string following the notifier format, takes precedence over notifier-from-file")
	stackingRule      = flag.String("stacking_rule", "current", "the stacking rule for notifier files (current, hourly, or daily)")
	submittingProgram = flag.String("submitting_program", "goSubmit", "name of the program/process that is submitting the grids")
	manualSubmission  = flag.Bool("manual_submission", false, "pass along that we are a manual submission")
	priority          = flag.Int("priority", 5, "Sets the priority for this submission, range 0-10")
	ttl               = flag.Int("ttl", 0, "data lifetime in the database")
	batchCfg          = batchConfig{BatchKey: getBatchKey()}
	submissionType    = "auto"
	amqpUri           = ""
	targetRegions     = []string{"US", "EU"}
	configFile        = flag.String("config_target", "config.yaml", "filename that app looks for to provide the config values.")
	configDir         = flag.String("config_dir", fmt.Sprintf("%s/conf", getAppRoot()), "directory that app searches to find the configuration file.")
	SkipS3            = flag.Bool("skip_s3", false, "data will still be ingested but it will not be put into the configured s3 bucket.\nIf the data does not exist in the s3 bucket, the grid could potentially be written as all missing values")

	submitDc = flag.String("submit_dc", "", "Uses internal default for rabbitMQ host & s3 bucket. Allowed [qarth, pyke, braavos].\nIf -bucket is set, that value will be used instead of the default s3 bucket")
	//target = flag.String("target", "dev", "target envrionment, allowed: dev, prod, both")

	archiveIngest = flag.Bool("archive", false, fmt.Sprintf("If truthy, grids will be stored in the and ingested using the value from -bucket option or the default archive s3 bucket, \"%s\"", ARCHIVE_BUCKET))
	archiveOnly   = flag.Bool("archive-only", false, fmt.Sprintf("If truthy, grids will not be ingested but only put in to the value from -bucket option or default archive s3 bucket, \"%s\"", ARCHIVE_BUCKET))
	keyspace      = flag.String("keyspace", "DEBUG", "wxgrid_long_lived_v2 | soil_model_v5")
	NoCompress    = flag.Bool("nocompress", false, "do not compress gridded data with zlib.\n**Uncompressed data is not supported yet in the downstream ingest writer process**")
	dryrun        = flag.Bool("dryrun", false, "used for debug/testing. Does not ingest data or put data in s3 bucket")
	//conf         *Config
	// define some "global" variables
	cfg       Config
	isArchive bool
	// archive key format .../YYYY/MM/ref_time.type[.zz]
	archivekeyformat = "%s/%d.%d.%d.%d/%d.%d/%d.%d.%d.%d.%d.%d/%d/%02d/%02d/%d.%s"
	// non archive key format .../ref_time.type[.zz]
	keyformat = "%s/%d.%d.%d.%d/%d.%d/%d.%d.%d.%d.%d.%d/%d.%s"
)

type GridMeta struct {
	ColWidth         int     `fig:"col_width"`
	GridSize         float64 `fig:"grid_size"`
	MaxGridsPerStack int     `fig:"max_grids_per_stack"`
	RowLength        int     `fig:"row_length"`
}

//goland:noinspection GoVetStructTag
type Config struct {
	Version string
	Build   time.Time
	App     struct {
		Environment string
	}
	Data struct {
		StackLimit        int `fig:"stack_count_limit" default: 1000`
		StackSizeLimit    int `fig:"stack_size_limit" default: 2048`
		DataIdWriteLength int `fig:"write_length" default: 20`
	}
	Grid map[int]GridMeta

	//Filter map[string]map[string]bool
	Filter map[string]map[string]bool
}

func init() {
	flag.Parse()
	ureg := strings.ToUpper(*region)
	region = &ureg
	if *archiveIngest || *archiveOnly {
		isArchive = true
	}
	if *region != "ALL" {
		targetRegions = []string{*region}
	}
	// use env vars if set
	if eRmqexchange, ok := os.LookupEnv("RABBITMQ_NOTIFIER_HEADER_EXCHANGE"); ok {
		defaultRmqExchange = eRmqexchange
	}
	if !*NoCompress {
		keyformat += ".zz"
		archivekeyformat += ".zz"
	}
	err := fig.Load(
		&cfg,
		fig.File(*configFile),
		fig.Dirs(".", "..", *configDir),
	)
	if err != nil {
		log.Fatal(err)
	}

	if *manualSubmission {
		submissionType = "manual"
	}
	// set archive bucket if -archive=true was provided
	if *S3Bucket == "" {
		if isArchive {
			bucket := ARCHIVE_BUCKET
			S3Bucket = &bucket
		}
	}
	if *submitDc != "" {
		sdc := strings.ToUpper(*submitDc)
		var buck, rmqh string
		if sdc == "QARTH" {
			buck = DEV_BUCKET
			rmqh = DEV_RMQ_HOST
		} else if sdc == "PYKE" {
			buck = PROD_BUCKET
			rmqh = US_RMQ_HOST
		} else if sdc == "BRAAVOS" {
			buck = PROD_BUCKET
			rmqh = EU_RMQ_HOST
		} else {
			log.Fatal(fmt.Sprintf("invalid option for submit_dc: %s", *submitDc))
		}
		if *S3Bucket == "" {
			S3Bucket = &buck
		}
		rmqHost = &rmqh
	}

	amqpUri = rmqutils.GetRmqUri(
		*rmqUser, *rmqPassword, *rmqHost, *rmqPort, *rmqVhost,
	)
	submitter := fmt.Sprintf("%s.%s", *submittingProgram, SUBMITTING_SUFFIX)
	submittingProgram = &submitter
	//conf = NewConfig()
	if *dryrun {
		log.WithFields(log.Fields{
			"TargetRegion":      targetRegions,
			"ManualSubmission":  *manualSubmission,
			"ArchiveIngest":     *archiveIngest,
			"ArchiveOnly":       *archiveOnly,
			"IsArchive":         isArchive,
			"SkipS3Upload":      *SkipS3,
			"S3Bucket":          *S3Bucket,
			"RMQ_URI":           amqpUri,
			"SubmittingProgram": submitter,
		}).Info("[dryrun] ingest configuration")
	}
}

func (g GridEvent) s3Key() string {
	if isArchive { // use archivekeyformat
		if g.RefTime <= 1001231 { // handle climo reftimes, ie 301213
			// climo year  -> 30 = 301213 / 10,000
			// month       -> 12 = 301213 / 100 % 100
			// day         -> 13 = 301213 % 100
			return fmt.Sprintf(
				archivekeyformat,
				*keyspace, g.Center, g.Subcenter, g.Grid, g.Process, g.ParamTableVer,
				g.Parameter, g.LevelType, g.Level0, g.Level1, g.TimeRange, g.Period0,
				g.Period1, g.RefTime/10000, g.RefTime/100%100, g.RefTime%100, g.RefTime, g.Type,
			)
		} else { // reftime is unix timestamp
			t := time.Unix(int64(g.RefTime), 0).UTC()
			return fmt.Sprintf(
				archivekeyformat,
				*keyspace, g.Center, g.Subcenter, g.Grid, g.Process, g.ParamTableVer,
				g.Parameter, g.LevelType, g.Level0, g.Level1, g.TimeRange, g.Period0,
				g.Period1, t.Year(), t.Month(), t.Day(), g.RefTime, g.Type,
			)
		}
	} else { // not archive so use keyformat
		return fmt.Sprintf(
			keyformat,
			*keyspace, g.Center, g.Subcenter, g.Grid, g.Process, g.ParamTableVer,
			g.Parameter, g.LevelType, g.Level0, g.Level1, g.TimeRange, g.Period0,
			g.Period1, g.RefTime, g.Type,
		)
	}
}

func (g GridEvent) metaTable() amqp.Table {
	return amqp.Table{
		"ref_time":        g.RefTime,
		"param_table_ver": g.ParamTableVer,
		"parameter":       g.Parameter,
		"level_type":      g.LevelType,
		"level0":          g.Level0,
		"level1":          g.Level1,
		"period0":         g.Period0,
		"period1":         g.Period1,
		"time_range":      g.TimeRange,
		"ttl":             *ttl,
		"type":            g.Type,
		//
		"wxevent_time": g.WxEventTime,
		"wxevent_type": g.WxEventType,
		"body_type":    "s3",
		//
		"forward_read_time": time.Now().Unix(),
	}
}

func (g *GridEvent) allowedToIngest(regions ...string) bool {
	// check conf if this can be ingested. If region is ALL check for existence, region filter will be applied later
	//when making the data stack
	k := fmt.Sprintf("%s.%d.%d.%d.%d", *keyspace, g.Center, g.Subcenter, g.Grid, g.Process)

	for _, reg := range regions {
		if _, allowed := cfg.Filter[k][reg]; allowed {
			g.allowedRegions = append(g.allowedRegions, reg)
		} else {
			g.allTargetRegions = false
		}
	}
	return len(g.allowedRegions) > 0
}

func (g GridEvent) headers() amqp.Table {
	// this will route to new cloud exchange which will have similiar behavior
	// as andal exchange. Shovel from cloud exchange to DCs (pyke & braavos)
	mqKey := fmt.Sprintf("%d.%d.%d", g.Center, g.Subcenter, g.Grid)
	if *region != "" {
		mqKey = fmt.Sprintf("%s.%s", mqKey, *region)
	}

	// this header will filter into queue on DC braavos/pyke
	dKey := fmt.Sprintf("%s.%s", *keyspace, g.Type)
	return amqp.Table{
		mqKey:      *keyspace,
		dKey:       *stackingRule,
		"metadata": g.metaTable(),
	}
}

type GridEvent struct {
	WxEventTime   int    `json:"wxevent_time"`
	WxEventType   string `json:"wxevent_type"`
	RefTime       int    `json:"ref_time"`
	Center        int    `json:"center,omitempty,string"`
	Subcenter     int    `json:"subcenter,omitempty,string"`
	Process       int    `json:"process,omitempty,string"`
	Grid          int    `json:"grid,omitempty,string"`
	ProductType   int    `json:"product_type,omitempty"`
	ProcessType   int    `json:"process_type,omitempty"`
	ParamTableVer int    `json:"param_table_ver,omitempty"`
	Parameter     int    `json:"parameter,omitempty"`
	LevelType     int    `json:"level_type,omitempty"`
	Level0        int    `json:"level0,omitempty"`
	Level1        int    `json:"level1,omitempty"`
	Period0       int    `json:"period0,omitempty"`
	Period1       int    `json:"period1,omitempty"`
	TimeRange     int    `json:"time_range,omitempty"`
	Type          string `json:"type,omitempty"`
	FieldIndex    int    `json:"field_index,omitempty"`
	BlobType      string `json:"blob_type,omitempty"`
	BlobName      string `json:"blob_name,omitempty"`
	ProductOffset int    `json:"product_offset,omitempty"`
	ProductSize   int    `json:"product_size,omitempty"`
	// stats
	S3UploadTime    float64 `json:"s3_upload_time,string"`
	S3UploadSize    float64 `json:"s3_upload_size,string"`
	SubmissionType  string  `json:"submission_type"`
	RmqQueue        string  `json:"rmq_queue"`
	ForwardReadTime float64 `json:"forward_read_time,string"`
	ReceiveReadTime float64 `json:"receive_read_time"`
	// used to write data/metadata
	GridLocation      string `json:"grid_location"`
	TTL               int    `json:"ttl"`
	DataIDWriteLength int    `json:"data_id_write_length"`

	ColWidth          int    `json:"col_width"`
	RowLength         int    `json:"row_length"`
	GridSource        string `json:"grid_source"` // s3
	SubmittingProgram string `json:"submitting_program"`
	//usless but needs to be deprecated in other services
	TileSize         int `json:"tile_size"` //not used but needs to be 1
	allowedRegions   []string
	allTargetRegions bool // allowed to all regions?
}

func toInt(val string) int {
	nv, _ := strconv.Atoi(val)
	return nv
}
func newGridEvent(sevent string) *GridEvent {
	s := strings.Split(sevent, " ")
	if len(s) == 3 { // decode but results in nop, skipping processing
		return &GridEvent{
			WxEventTime: toInt(s[0]),
			WxEventType: s[1],
			RefTime:     toInt(s[2]),
		}
	} else { // decode and process mesage
		return &GridEvent{
			WxEventTime:   toInt(s[0]),
			WxEventType:   s[1],
			RefTime:       toInt(s[2]),
			Center:        toInt(s[3]),
			Subcenter:     toInt(s[4]),
			Process:       toInt(s[5]),
			Grid:          toInt(s[6]),
			ProductType:   toInt(s[7]),
			ProcessType:   toInt(s[8]),
			ParamTableVer: toInt(s[9]),
			Parameter:     toInt(s[10]),
			LevelType:     toInt(s[11]),
			Level0:        toInt(s[12]),
			Level1:        toInt(s[13]),
			Period0:       toInt(s[14]),
			Period1:       toInt(s[15]),
			TimeRange:     toInt(s[16]),
			Type:          s[17],
			FieldIndex:    toInt(s[18]),
			BlobType:      s[19],
			BlobName:      filepath.Clean(*defaultPath + s[20]),
			ProductOffset: toInt(s[21]),
			ProductSize:   toInt(s[22]),
			// non notifier event
			TTL:               *ttl,
			SubmittingProgram: *submittingProgram,
			TileSize:          1,
			GridSource:        "s3",
			SubmissionType:    submissionType,
			DataIDWriteLength: cfg.Data.DataIdWriteLength,
			allTargetRegions:  true,
			allowedRegions:    []string{},
		}
	}
}

type batchConfig struct {
	Grid     GridMeta
	BatchKey string
}

func getBatchKey() string {
	return uuid.New().String()
}

func ingestForwardEvent(notifier_event string, out chan<- GridEvent) {
	defer close(out)
	batchKey := getBatchKey()

	event := newGridEvent(strings.TrimSpace(notifier_event))
	if event.allowedToIngest(targetRegions...) {
		event.ForwardReadTime = float64(time.Now().UnixNano()) / 1000000000
		out <- *event
	} else {
		log.WithFields(log.Fields{
			"S3Key":            event.s3Key(),
			"batch_unique_key": batchKey,
			//"headers":          event.headers(),
			"blob_name": event.BlobName,
		}).Warn("skipping event, not allowed")
	}
}

func ingestForward(submitFileName string, out chan<- GridEvent) {
	defer close(out)
	notifier, err := os.Open(submitFileName)
	if err != nil {
		log.Fatal(err)
	}
	defer notifier.Close()
	batchKey := getBatchKey()
	scanner := bufio.NewScanner(notifier)

	for scanner.Scan() {
		event := newGridEvent(scanner.Text())
		if event.allowedToIngest(targetRegions...) {
			event.ForwardReadTime = float64(time.Now().UnixNano()) / 1000000000
			out <- *event
		} else {
			log.WithFields(log.Fields{
				"S3Key":            event.s3Key(),
				"batch_unique_key": batchKey,
				//"headers":          event.headers(),
				"blob_name": event.BlobName,
			}).Warn("skipping event, not allowed")
		}
	}
}

func ingestPusher(uploader *datapusher.IngestUploader, events <-chan GridEvent, out chan<- GridEvent, wg *sync.WaitGroup) {
	// for each input file: compress; push to s3; pass to ingestReceive
	defer wg.Done()
	for event := range events {
		start := time.Now()
		s3Key := event.s3Key()
		uploadSize, err := uploader.UploadToS3(s3Key, event.BlobName)
		if err != nil {
			log.WithFields(log.Fields{
				"S3Bucket":   S3Bucket,
				"S3Key":      s3Key,
				"fileToPush": event.BlobName,
			}).Error(err)
			continue // don't add to stack
		} else if *dryrun {
			if *SkipS3 {
				log.WithFields(log.Fields{
					"S3Key": fmt.Sprintf("s3://%s/%s", S3Bucket, s3Key),
				}).Info("[dryrun] using existing s3 object")
			} else {
				log.WithFields(log.Fields{
					"S3Bucket":   *S3Bucket,
					"S3Key":      s3Key,
					"fileToPush": event.BlobName,
				}).Info("[dryrun] upload to s3")
			}
		} else if *SkipS3 {
			log.WithFields(log.Fields{
				"S3Key": fmt.Sprintf("s3://%s/%s", S3Bucket, s3Key),
			}).Debug("[dryrun] using existing s3 object")
		} else {
			log.WithFields(log.Fields{
				"S3Bucket":   S3Bucket,
				"S3Key":      s3Key,
				"fileToPush": event.BlobName,
				"UploadSize": uploadSize,
			}).Debug("upload to s3")

		}
		event.S3UploadSize = float64(uploadSize)
		event.S3UploadTime = time.Now().Sub(start).Seconds()
		event.GridLocation = s3Key
		out <- event
	}
}

type MessageBody struct {
	//container for collecting events and dumping to json
	targetRegion string
	maxStackSize int
	Batch        struct {
		BatchUniqueKey string      `json:"batch_unique_key"`
		Grids          []GridEvent `json:"grids"`
	} `json:"batch"`
	Stats struct {
		Args struct {
			TTL      int    `json:"ttl"`
			Keyspace string `json:"keyspace"`
			GridType string `json:"grid_type"`
			TargetDc string `json:"rabbitmq_host"`
			S3Bucket string `json:"s3_bucket,omitempty"`
		} `json:"args"`
	} `json:"stats"`
}

func newMessageBody(targetRegion string) *MessageBody {
	mbody := MessageBody{targetRegion: targetRegion}
	mbody.Batch.Grids = []GridEvent{} // init the array
	mbody.Batch.BatchUniqueKey = batchCfg.BatchKey
	mbody.Stats.Args.TTL = *ttl
	mbody.Stats.Args.Keyspace = *keyspace
	mbody.Stats.Args.TargetDc = targetRegion
	mbody.Stats.Args.S3Bucket = *S3Bucket
	return &mbody
}

type receiveStacks struct {
	stacks map[string]*MessageBody
}

func newReceiveStacks() *receiveStacks {
	dstacks := receiveStacks{}
	dstacks.stacks = make(map[string]*MessageBody)
	for _, r := range targetRegions {
		dstacks.stacks[r] = newMessageBody(r)
	}
	return &dstacks
}
func (rs *receiveStacks) addEvent(event GridEvent) {
	for _, reg := range event.allowedRegions {
		if mbody, ok := rs.stacks[reg]; ok {
			mbody.Batch.Grids = append(mbody.Batch.Grids, event)
		}
	}
}

func (mbody *MessageBody) flush() *rmqutils.Message {
	mbody.Stats.Args.GridType = mbody.Batch.Grids[0].Type
	jbody, _ := json.Marshal(mbody)
	// clear the batch
	mbody.Batch.Grids = []GridEvent{}
	return rmqutils.NewIngestMessage(
		jbody,
		mbody.targetRegion,
		mbody.Stats.Args.Keyspace,
		"cloud_submit",
		mbody.Batch.BatchUniqueKey,
		*priority,
	)

}

func ingestReceive(events <-chan GridEvent, out chan<- *rmqutils.Message) {
	//listen for output from ingestPusher
	// batch into datastacks for one or both EU, US
	// send RMQ message to global queue (filter queue)
	// Eventually ends up in ingest_writer queue in DC
	defer close(out)
	maxStackSize := 0
	dstacks := newReceiveStacks()

	// make a MessageBody for each target region
	for _, reg := range targetRegions {
		dstacks.stacks[reg] = newMessageBody(reg)
	}

	for event := range events {
		event.ReceiveReadTime = float64(time.Now().UnixNano()) / 100000000
		if maxStackSize == 0 {
			gmeta, ok := cfg.Grid[event.Grid]
			if !ok {
				log.Fatal(fmt.Sprintf("Grid %d not configured for ingest", event.Grid))
			}
			batchCfg.Grid = gmeta
			maxStackSize = batchCfg.Grid.MaxGridsPerStack

		}

		// process, set meta
		event.RowLength = batchCfg.Grid.RowLength
		event.ColWidth = batchCfg.Grid.ColWidth
		for _, tregion := range event.allowedRegions {
			if stack, ok := dstacks.stacks[tregion]; ok {
				stack.Batch.Grids = append(stack.Batch.Grids, event)
				if len(stack.Batch.Grids) == maxStackSize {
					if *dryrun {
						log.WithFields(log.Fields{
							"Region":      tregion,
							"StackHeight": len(stack.Batch.Grids),
						}).Info("[dryrun] stack full, flushing")
					}
					out <- stack.flush()
				}
			}
		}
	}
	for _, stack := range dstacks.stacks { // check if we need to flush
		if len(stack.Batch.Grids) > 0 {
			// flush to rmq
			out <- stack.flush()
		}
	}
}

func stackFlusher(datastacks <-chan *rmqutils.Message, done chan<- bool) {
	// send message to RMQ
	defer close(done)
	if *archiveOnly {
		log.Info("[archiveOnly] Skipping ingest of the datastack")
		for _ = range datastacks { /*drain and do nothing*/
		}

	} else if *dryrun { // just print stack
		for stack := range datastacks {
			ds := MessageBody{}
			json.Unmarshal(stack.Body, &ds)
			log.WithFields(log.Fields{
				"RMQHeaders":  stack.Headers,
				"RMQBody":     fmt.Sprintf("%+v", ds),
				"RMQPriority": stack.Priority,
			}).Info("[dryrun] sending datastack to RabbitMq")
		}
	} else {
		rmqSesh := rmqutils.NewRmq("cloud_submit", amqpUri, *rmqExchange, targetRegions)
		for i := 0; i < 5; i++ {
			if rmqSesh.IsReady() {
				break
			}
			time.Sleep(1 * time.Second)
		}
		if !rmqSesh.IsReady() {
			log.WithFields(
				log.Fields{"rmq_uri": amqpUri},
			).Error("Failed to connect to RabbitMQ")
		}
		for stack := range datastacks {
			if err := rmqSesh.Push(stack); err != nil {
				log.Error(err)
			}
			//log.Info(stack)
		}
	}
	done <- true
}

func batchIngest() chan bool {
	pusherQueue := make(chan GridEvent)
	receiveQueue := make(chan GridEvent)
	datastackQueue := make(chan *rmqutils.Message)
	done := make(chan bool, 1)

	if *notifierEvent != "" {
		go ingestForwardEvent(*notifierEvent, pusherQueue)
		*workers = 1
	} else {
		go ingestForward(*notifierFile, pusherQueue)
	}

	var pushers sync.WaitGroup
	var storageClass string
	if isArchive {
		storageClass = "STANDARD_IA"
	} else {
		storageClass = "STANDARD"
	}
	var svc = datapusher.NewIngestUploader(
		*S3Bucket,
		*S3Region,
		storageClass,
		*S3Profile,
		S3Timeout,
		*SkipS3,
		*dryrun,
		*NoCompress,
	)

	for i := 0; i < *workers; i++ {
		pushers.Add(1)
		go ingestPusher(svc, pusherQueue, receiveQueue, &pushers)
	}
	go func() { // wait and close the queue in the background
		pushers.Wait()
		close(receiveQueue)
	}()

	go ingestReceive(receiveQueue, datastackQueue)

	go stackFlusher(datastackQueue, done)

	return done
}

func main() {
	<-batchIngest()
	log.Debug("shutting down")
}
