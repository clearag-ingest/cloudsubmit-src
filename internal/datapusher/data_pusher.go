package datapusher

import (
	"bytes"
	"compress/zlib"
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type IngestUploader struct {
	Dryrun       bool
	SkipS3       bool
	storageClass string
	compress     bool
	s3Svc        *s3manager.Uploader
	sesh         *session.Session
	bucket       *string
	timeout      *time.Duration
}

//goland:noinspection GoUnusedExportedFunction
func NewIngestUploader(bucket, region, storageClass, profile string, timeout *time.Duration, skipS3, dryrun, noCompress bool) *IngestUploader {

	var svc = IngestUploader{
		SkipS3:       skipS3,
		Dryrun:       dryrun,
		compress:     !noCompress,
		timeout:      timeout,
		storageClass: storageClass,
	}
	var s3opts = session.Options{
		Config: aws.Config{
			Region: aws.String(region),
		},
		SharedConfigState: session.SharedConfigEnable,
	}
	if profile != "" {
		s3opts.Profile = profile
	}
	if storageClass == "" {
		svc.storageClass = "STANDARD"
	}

	svc.bucket = aws.String(bucket)
	svc.sesh = session.Must(session.NewSessionWithOptions(s3opts))
	svc.s3Svc = s3manager.NewUploader(svc.sesh)

	return &svc
}

func (svc *IngestUploader) UploadToS3(key, filename string) (uploadSize int, err error) {
	// Create a context with a timeout that will abort the upload if it takes
	// more than the passed in timeout.
	if svc.SkipS3 || svc.Dryrun { // we are testing or data is already in s3
		uploadSize = 0
		//err = fmt.Errorf("Skipping s3 upload")
		return
	}

	f, err := os.Open(filename)
	if err != nil {
		return
	}
	defer f.Close()

	ctx := context.Background()
	var cancelFn func()
	if *svc.timeout > 0 {
		ctx, cancelFn = context.WithTimeout(ctx, *svc.timeout)
	}
	// Ensure the context is canceled to prevent leaking.
	// See context package for more information, https://golang.org/pkg/context/
	if cancelFn != nil {
		defer cancelFn()
	}

	// Uploads the object to S3. The Context will interrupt the request if the
	// timeout expires.

	if svc.compress { // compress and upload
		var b bytes.Buffer
		w := zlib.NewWriter(&b)
		inB, err := ioutil.ReadAll(f)
		if err != nil {
			return 0, err
		}
		uploadSize, _ = w.Write(inB) // write to our bytes buffer
		w.Close()                    // assure data is flushed
		dout := b.Bytes()
		_, err = svc.s3Svc.UploadWithContext(ctx, &s3manager.UploadInput{
			ACL:          aws.String("bucket-owner-full-control"),
			Bucket:       svc.bucket,
			Key:          aws.String(key),
			Body:         bytes.NewReader(dout),
			RequestPayer: aws.String("requester"),
			StorageClass: aws.String(svc.storageClass),
		})
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case request.CanceledErrorCode:
					// If the SDK can determine the request or retry delay was canceled
					// by a context the CanceledErrorCode error code will be returned.
					log.Fatal("upload canceled due to timeout, %v", err)
					break
				case "AccessDenied":
					creds, cerr := svc.sesh.Config.Credentials.Get()
					log.WithFields(log.Fields{
						"AccessKeyID":      creds.AccessKeyID,
						"SecretAccessKey":  creds.SecretAccessKey,
						"SessionToken":     creds.SessionToken,
						"ProviderName":     creds.ProviderName,
						"credential_error": cerr,
						"region":           *svc.sesh.Config.Region,
						"bucket":           *svc.bucket,
					}).Fatal(err)
					break
				default:
					return 0, err
				}
			}
		}
	} else { // copy file to bucket
		fi, err := f.Stat()
		if err != nil {
			log.Warning(fmt.Sprintf("could not stat grid file %s", filename))
		} else {
			uploadSize = int(fi.Size())
		}
		_, err = svc.s3Svc.UploadWithContext(ctx, &s3manager.UploadInput{
			ACL:    aws.String("bucket-owner-full-control"),
			Bucket: svc.bucket,
			Key:    aws.String(key),
			Body:   f,
		})
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case request.CanceledErrorCode:
					// If the SDK can determine the request or retry delay was canceled
					// by a context the CanceledErrorCode error code will be returned.
					log.Fatal("upload canceled due to timeout, %v", err)
					break
				case "AccessDenied":
					creds, cerr := svc.sesh.Config.Credentials.Get()
					log.WithFields(log.Fields{
						"AccessKeyID":      creds.AccessKeyID,
						"SecretAccessKey":  creds.SecretAccessKey,
						"SessionToken":     creds.SessionToken,
						"ProviderName":     creds.ProviderName,
						"credential_error": cerr,
						"region":           *svc.sesh.Config.Region,
						"bucket":           *svc.bucket,
					}).Fatal(err)
					break
				default:
					return 0, err
				}
			}
		}
	}
	return
}
