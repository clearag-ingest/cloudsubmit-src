module rmqutils

go 1.15

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/streadway/amqp v1.0.0
)
