package rmqutils

import (
	"errors"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

const (
	// When reconnecting to the server after connection failure
	reconnectDelay = 5 * time.Second

	// When setting up the channel after a channel exception
	reInitDelay = 2 * time.Second

	// When resending messages the server didn't confirm
	resendDelay = 5 * time.Second
)

var (
	errNotConnected  = errors.New("not connected to a server")
	errAlreadyClosed = errors.New("already closed: not connected to the server")
	errShutdown      = errors.New("session is shutting down")
)

// RMQ publish
type Session struct {
	name            string
	logger          *log.Logger
	connection      *amqp.Connection
	channel         *amqp.Channel
	exchange        string
	done            chan bool
	notifyConnClose chan *amqp.Error
	notifyChanClose chan *amqp.Error
	notifyConfirm   chan amqp.Confirmation
	isReady         bool
	targetRegions   []string
}

func (session *Session) IsReady() bool {
	return session.isReady
}

type Message struct {
	Body     []byte
	Headers  amqp.Table
	Priority int
}

func NewIngestMessage(body []byte, targetRegion, keyspace, ident, batchKey string, priority int) *Message {

	return &Message{
		Body: body,
		Headers: amqp.Table{
			targetRegion:   "cloud_submit",
			"keyspace":     keyspace,
			"status_ident": ident,
			"key":          batchKey,
		},
		Priority: priority,
	}

}

func GetRmqUri(user, password, host string, port int, virtualHost string) string {
	return fmt.Sprintf(
		"amqp://%s:%s@%s:%d/%s", user, password, host, port, virtualHost,
	)
}

// New creates a new consumer state instance, and automatically
// attempts to connect to the server.
//goland:noinspection GoUnusedExportedFunction
func NewRmq(name, addr, exchange string, targetRegions []string) *Session {
	session := Session{
		logger:        log.New(),
		name:          name,
		done:          make(chan bool),
		targetRegions: targetRegions,
		exchange:      exchange,
	}
	go session.handleReconnect(addr)
	return &session
}

// handleReconnect will wait for a connection error on
// notifyConnClose, and then continuously attempt to reconnect.
func (session *Session) handleReconnect(addr string) {
	for {
		session.isReady = false
		log.Debug("Attempting to connect")

		conn, err := session.connect(addr)

		if err != nil {
			log.Error(err)
			log.Debug("Failed to connect. Retrying...")

			select {
			case <-session.done:
				return
			case <-time.After(reconnectDelay):
			}
			continue
		}

		if done := session.handleReInit(conn); done {
			break
		}
	}
}

// connect will create a new AMQP connection
func (session *Session) connect(addr string) (*amqp.Connection, error) {
	conn, err := amqp.Dial(addr)

	if err != nil {
		return nil, err
	}

	session.changeConnection(conn)
	log.Debug("Connected!")
	return conn, nil
}

// handleReconnect will wait for a channel error
// and then continuously attempt to re-initialize both channels
func (session *Session) handleReInit(conn *amqp.Connection) bool {
	for {
		session.isReady = false

		err := session.init(conn)

		if err != nil {
			log.Println("Failed to initialize channel. Retrying...")

			select {
			case <-session.done:
				return true
			case <-time.After(reInitDelay):
			}
			continue
		}

		select {
		case <-session.done:
			return true
		case <-session.notifyConnClose:
			log.Debug("Connection closed. Reconnecting...")
			return false
		case <-session.notifyChanClose:
			log.Debug("Channel closed. Re-running init...")
		}
	}
}

// init will initialize channel & declare queue
//goland:noinspection GoUnhandledErrorResult
func (session *Session) init(conn *amqp.Connection) error {
	ch, err := conn.Channel()

	if err != nil {
		return err
	}

	err = ch.Confirm(false)

	if err != nil {
		return err
	}

	for _, reg := range session.targetRegions {
		qname := fmt.Sprintf("%s.%s", session.exchange, reg)
		_, err = ch.QueueDeclare(
			qname,
			true,                             // Durable
			false,                            // Delete when unused
			false,                            // Exclusive
			false,                            // No-wait
			amqp.Table{"x-max-priority": 10}, // Arguments
		)
		if err != nil {
			return err
		}
		ch.QueueBind(qname, "", session.exchange, false, amqp.Table{reg: "cloud_submit"})
	}

	session.changeChannel(ch)
	session.isReady = true
	log.Debug("Setup!")

	return nil
}

// changeConnection takes a new connection to the queue,
// and updates the close listener to reflect this.
func (session *Session) changeConnection(connection *amqp.Connection) {
	session.connection = connection
	session.notifyConnClose = make(chan *amqp.Error)
	session.connection.NotifyClose(session.notifyConnClose)
}

// changeChannel takes a new channel to the queue,
// and updates the channel listeners to reflect this.
func (session *Session) changeChannel(channel *amqp.Channel) {
	session.channel = channel
	session.notifyChanClose = make(chan *amqp.Error)
	session.notifyConfirm = make(chan amqp.Confirmation, 1)
	session.channel.NotifyClose(session.notifyChanClose)
	session.channel.NotifyPublish(session.notifyConfirm)
}

// Push will push data onto the queue, and wait for a confirm.
// If no confirms are received until within the resendTimeout,
// it continuously re-sends messages until a confirm is received.
// This will block until the server sends a confirm. Errors are
// only returned if the push action itself fails, see UnsafePush.
func (session *Session) Push(data *Message) error {
	if !session.isReady {
		return errors.New("failed to push: not connected")
	}
	for {
		err := session.UnsafePush(data)
		if err != nil {
			session.logger.Warn("Push failed. Retrying...")
			select {
			case <-session.done:
				return errShutdown
			case <-time.After(resendDelay):
			}
			continue
		}
		select {
		case confirm := <-session.notifyConfirm:
			if confirm.Ack {
				session.logger.Debug("Push confirmed!")
				return nil
			}
		case <-time.After(resendDelay):
		}
		session.logger.Warn("Push didn't confirm. Retrying...")
	}
}

// UnsafePush will push to the queue without checking for
// confirmation. It returns an error if it fails to connect.
// No guarantees are provided for whether the server will
// receive the message.
func (session *Session) UnsafePush(msg *Message) error {
	if !session.isReady {
		return errNotConnected
	}
	return session.channel.Publish(
		session.exchange, // Exchange
		"",               // Routing key
		false,            // Mandatory
		false,            // Immediate
		amqp.Publishing{
			Headers:      msg.Headers,
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         msg.Body,
			Priority:     uint8(msg.Priority),
		},
	)
}

// Close will cleanly shutdown the channel and connection.
func (session *Session) Close() error {
	if !session.isReady {
		return errAlreadyClosed
	}
	err := session.channel.Close()
	if err != nil {
		return err
	}
	err = session.connection.Close()
	if err != nil {
		return err
	}
	close(session.done)
	session.isReady = false
	return nil
}
