package gridevents
import (
    "path/filepath"
    "strconv"
    "strings"
    "time"
)

var (
    keyformat = "%s/%d.%d.%d.%d/%d.%d/%d.%d.%d.%d.%d.%d/%d.%s"
    eventKeys = [23]string{
        "wxevent_time", "wxevent_type", "ref_time", "center", "subcenter",
        "process", "grid", "product_type", "process_type", "param_table_ver",
        "parameter", "level_type", "level0", "level1", "period0", "period1",
        "time_range", "type", "field_index", "blob_type", "blob_name",
        "product_offset", "product_size",
    }
    submissionType string
)

type GridMeta struct {
    ColWidth         int     `fig:"col_width"`
    GridSize         float64 `fig:"grid_size"`
    MaxGridsPerStack int     `fig:"max_grids_per_stack"`
    RowLength        int     `fig:"row_length"`
}

//goland:noinspection GoVetStructTag
type Config struct {
    Version string
    Build   time.Time
    App     struct {
        Environment string
    }
    Data struct {
        StackLimit        int `fig:"stack_count_limit" default: 1000`
        StackSizeLimit    int `fig:"stack_size_limit" default: 2048`
        DataIdWriteLength int `fig:"write_length" default: 20`
    }
    Grid map[int]GridMeta

    //Filter map[string]map[string]bool
    Filter map[string]map[string]bool
}

var cfg Config

func (g GridEvent) s3Key() string {
    return fmt.Sprintf(
        keyformat,
        *keyspace, g.Center, g.Subcenter, g.Grid, g.Process, g.ParamTableVer,
        g.Parameter, g.LevelType, g.Level0, g.Level1, g.TimeRange, g.Period0,
        g.Period1, g.RefTime, g.Type,
    )
}

func (g GridEvent) metaTable() amqp.Table {
    return amqp.Table{
        "ref_time":        g.RefTime,
        "param_table_ver": g.ParamTableVer,
        "parameter":       g.Parameter,
        "level_type":      g.LevelType,
        "level0":          g.Level0,
        "level1":          g.Level1,
        "period0":         g.Period0,
        "period1":         g.Period1,
        "time_range":      g.TimeRange,
        "ttl":             *ttl,
        "type":            g.Type,
        //
        "wxevent_time": g.WxEventTime,
        "wxevent_type": g.WxEventType,
        "body_type":    "s3",
        //
        "forward_read_time": time.Now().Unix(),
    }
}

func (g *GridEvent) allowedToIngest(regions ...string) bool {
    // check conf if this can be ingested. If region is ALL check for existence, region filter will be applied later
    //when making the data stack
    k := fmt.Sprintf("%s.%d.%d.%d.%d", *keyspace, g.Center, g.Subcenter, g.Grid, g.Process)

    for _, reg := range regions {
        if _, allowed := cfg.Filter[k][reg]; allowed {
            g.allowedRegions = append(g.allowedRegions, reg)
        } else {
            g.allTargetRegions = false
        }
    }
    return len(g.allowedRegions) > 0
}


type GridEvent struct {
    WxEventTime   int    `json:"wxevent_time"`
    WxEventType   string `json:"wxevent_type"`
    RefTime       int    `json:"ref_time"`
    Center        int    `json:"center,omitempty,string"`
    Subcenter     int    `json:"subcenter,omitempty,string"`
    Process       int    `json:"process,omitempty,string"`
    Grid          int    `json:"grid,omitempty,string"`
    ProductType   int    `json:"product_type,omitempty"`
    ProcessType   int    `json:"process_type,omitempty"`
    ParamTableVer int    `json:"param_table_ver,omitempty"`
    Parameter     int    `json:"parameter,omitempty"`
    LevelType     int    `json:"level_type,omitempty"`
    Level0        int    `json:"level0,omitempty"`
    Level1        int    `json:"level1,omitempty"`
    Period0       int    `json:"period0,omitempty"`
    Period1       int    `json:"period1,omitempty"`
    TimeRange     int    `json:"time_range,omitempty"`
    Type          string `json:"type,omitempty"`
    FieldIndex    int    `json:"field_index,omitempty"`
    BlobType      string `json:"blob_type,omitempty"`
    BlobName      string `json:"blob_name,omitempty"`
    ProductOffset int    `json:"product_offset,omitempty"`
    ProductSize   int    `json:"product_size,omitempty"`
    // stats
    S3UploadTime    float64 `json:"s3_upload_time,string"`
    S3UploadSize    float64 `json:"s3_upload_size,string"`
    SubmissionType  string  `json:"submission_type"`
    RmqQueue        string  `json:"rmq_queue"`
    ForwardReadTime float64 `json:"forward_read_time,string"`
    ReceiveReadTime float64 `json:"receive_read_time"`
    // used to write data/metadata
    GridLocation      string `json:"grid_location"`
    TTL               int    `json:"ttl"`
    DataIDWriteLength int    `json:"data_id_write_length"`

    ColWidth          int    `json:"col_width"`
    RowLength         int    `json:"row_length"`
    GridSource        string `json:"grid_source"` // s3
    SubmittingProgram string `json:"submitting_program"`
    //usless but needs to be deprecated in other services
    TileSize         int `json:"tile_size"` //not used but needs to be 1
    allowedRegions   []string
    allTargetRegions bool // allowed to all regions?
}

func toInt(val string) int {
    nv, _ := strconv.Atoi(val)
    return nv
}
func newGridEvent(sevent, defaultPath, submittingProgram, submissionType string, ttl int) *GridEvent {
    s := strings.Split(sevent, " ")
    if len(s) == 3 {
        return &GridEvent{WxEventTime: toInt(s[0]), WxEventType: s[1], RefTime: toInt(s[2])}
    } else {
        return &GridEvent{WxEventTime: toInt(s[0]), WxEventType: s[1], RefTime: toInt(s[2]), Center: toInt(s[3]),
            Subcenter: toInt(s[4]), Process: toInt(s[5]), Grid: toInt(s[6]), ProductType: toInt(s[7]),
            ProcessType: toInt(s[8]), ParamTableVer: toInt(s[9]), Parameter: toInt(s[10]), LevelType: toInt(s[11]),
            Level0: toInt(s[12]), Level1: toInt(s[13]), Period0: toInt(s[14]), Period1: toInt(s[15]),
            TimeRange: toInt(s[16]), Type: s[17], FieldIndex: toInt(s[18]), BlobType: s[19],
            BlobName: filepath.Clean(defaultPath + s[20]), ProductOffset: toInt(s[21]), ProductSize: toInt(s[22]),
            TTL: ttl, SubmittingProgram: submittingProgram, TileSize: 1, GridSource: "s3",
            SubmissionType: submissionType, DataIDWriteLength: cfg.Data.DataIdWriteLength, allTargetRegions: true, allowedRegions: []string{},
        }

    }
}

