package main

import (
	//"bytes"
	"encoding/json"
	"fmt"
	//"os"
	"strings"
	"time"

	cql "github.com/gocql/gocql"
	"github.com/kkyr/fig"
	log "github.com/sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"
	//"github.com/tleben/GoCQLEC2MultiRegionTranslator/gocqlec2multiregiontranslator"
)

type GridSpecConfig struct {
	Center           int
	ColWidth         int
	Grid             int
	GridSize         float64
	Processes        []int
	RowLength        int
	Subcenter        int
	MaxGridsPerStack int
}

func NewGridSpecConfig(r map[string]interface{}) *GridSpecConfig {
	var payload map[string]interface{}
	if err := json.Unmarshal([]byte(r["payload"].(string)), &payload); err != nil {
		log.Fatal(err)
	}
	gs := GridSpecConfig{
		Center:    r["center"].(int),
		ColWidth:  int(payload["col_count"].(float64)),
		Grid:      int(r["grid"].(int)),
		RowLength: int(payload["row_count"].(float64)),
		Subcenter: int(r["subcenter"].(int)),
	}
	for _, val := range payload["processes"].([]interface{}) {
		gs.Processes = append(gs.Processes, int(val.(float64)))
	}
	gs.GridSize = float64(gs.RowLength*gs.ColWidth*4.0) / 1000000.0 // 4 byte float

	gs.MaxGridsPerStack = int(float64(cfg.StackConf.StackSizeLimit) / gs.GridSize)
	if gs.MaxGridsPerStack > cfg.StackConf.StackLimit {
		gs.MaxGridsPerStack = cfg.StackConf.StackLimit
	}
	if gs.MaxGridsPerStack == 0 {
		gs.MaxGridsPerStack = 1
	}
	return &gs
}

type Config struct {
	Version   string `fig:"version" default:"-1"`
	StackConf struct {
		StackLimit        int `fig:"max_grids" default: 1000`
		StackSizeLimit    int `fig:"max_size" default: 2048`
		DataIdWriteLength int `fig:"write_length" default: 20`
	} `fig:"stack_conf"`
	GridFilter map[string]map[string]map[int]bool `fig:"grid_filter"`
	Cassandra  struct {
		Keyspaces []string            `fig:"keyspaces" validate:"required"`
		Regions   map[string]string   `validate:"required"`
		Nodes     map[string][]string `fig:"nodes" validate:"required"`
	}
}

func (c *Config) gridAllowed(region, ks string, grid_id int) bool {
	if filter, ok := c.GridFilter[region][ks]; ok { // we have a filter configured
		// filter is configured for region keyspace pair, use value configured
		return filter[grid_id]
	}
	// no filter, so default to true
	return true
}

var (
	cfg Config
)

func init() {
	err := fig.Load(&cfg, fig.File("config/config.yaml"))
	if err != nil {
		log.Fatal(err)
	}
	//log.Info(cfg.GridFilter)
}

func GetWxgridNodes(dc string) (wxgridNodes []string) {
	wxgridNodes, _ = cfg.Cassandra.Nodes[dc]
	return
}

func getClusterCfg(dc string) (cluster *cql.ClusterConfig) {
	//log.Info(GetWxgridNodes(dc))
	cluster = cql.NewCluster(GetWxgridNodes(dc)...)
	cluster.ConnectTimeout = 1600 * time.Millisecond // default 600ms
	cluster.Timeout = 15 * time.Second               // default 600ms
	cluster.Consistency = cql.Quorum                 //Quorum
	cluster.Compressor = &cql.SnappyCompressor{}
	//cluster.RetryPolicy = &cql.ExponentialBackoffRetryPolicy{Min: 100 * time.Millisecond, Max: 3 * time.Second, NumRetries: 3}
	cluster.RetryPolicy = &cql.DowngradingConsistencyRetryPolicy{ConsistencyLevelsToTry: []cql.Consistency{cql.Quorum, cql.Quorum, cql.Quorum, cql.One, cql.One}}
	cluster.PoolConfig.HostSelectionPolicy = cql.TokenAwareHostPolicy(cql.RoundRobinHostPolicy(), cql.ShuffleReplicas())
	cluster.ReconnectionPolicy = &cql.ConstantReconnectionPolicy{MaxRetries: 10, Interval: 8 * time.Second}
	cluster.IgnorePeerAddr = true
	cluster.DisableInitialHostLookup = true

	if dc == "braavos" {
		cluster.SslOpts = &cql.SslOptions{
			CaPath: "./cert/cass.crt",
		}
		cluster.Authenticator = cql.PasswordAuthenticator{
			Username: "cassandra",
			Password: "cassandra",
		}

	}
	return
}

func GridSpecs(ks string, sesh *cql.Session) ([]map[string]interface{}, error) {
	qstr := "SELECT * from %s_meta.grid_specs;"
	iter := sesh.Query(fmt.Sprintf(qstr, ks)).Iter()
	gs, err := iter.SliceMap()
	return gs, err
}

func getDataConf(region, ks string, sesh *cql.Session) []*GridSpecConfig {
	gs, err := GridSpecs(ks, sesh)
	if err != nil {
		log.Fatal(err)
	}
	ret := []*GridSpecConfig{}
	for _, r := range gs {
		// check filter
		if cfg.gridAllowed(region, ks, r["grid"].(int)) {
			ret = append(ret, NewGridSpecConfig(r))
		}
	}
	return ret
}

func getRegionConf(region, dc string) map[string][]*GridSpecConfig {
	ret := map[string][]*GridSpecConfig{}
	cluster := getClusterCfg(dc)
	sesh, err := cluster.CreateSession()
	if err != nil {
		log.Fatal(err)
	}
	defer sesh.Close()
	for _, ks := range cfg.Cassandra.Keyspaces {
		ret[ks] = getDataConf(region, ks, sesh)
		//gridConf := getDataConf(region, ks, sesh)
		//for _, r := range gridConf {
		//	fmt.Printf("%+v\n", *r)
		//}
	}

	return ret
}

//Center           int
//ColWidth         int
//Grid             int
//GridSize         float64
//Processes        []int
//RowLength        int
//Subcenter        int
//MaxGridsPerStack int

type GridMeta struct {
	ColWidth         int     `yaml:"col_width"`
	GridSize         float64 `yaml:"grid_size"`
	MaxGridsPerStack int     `yaml:"max_grids_per_stack"`
	RowLength        int     `yaml:"row_length"`
}

type DataFilter struct {
	Keyspace  string
	Center    int
	Subcenter int
	Grid      int
	Process   int
}
type DcFilter struct {
	EU bool `yaml:"EU,omitempty"`
	US bool `yaml:"US,omitempty"`
}
type outConfig struct {
	Version string
	Build   time.Time
	App     struct {
		Environment string
	}
	Data struct {
		StackLimit        int `yaml:"stack_count_limit"`
		StackSizeLimit    int `yaml:"stack_size_limit"`
		DataIdWriteLength int `yaml:"write_length"`
	}
	Grid map[int]GridMeta
	//Filter map[string]map[string]bool
	Filter map[string]map[string]bool
}

func makeFilterConfig() []byte {
	oconf := outConfig{Version: cfg.Version, Build: time.Now().UTC()}
	oconf.App.Environment = "production"
	oconf.Data.StackLimit = cfg.StackConf.StackLimit
	oconf.Data.StackSizeLimit = cfg.StackConf.StackSizeLimit
	oconf.Data.DataIdWriteLength = cfg.StackConf.DataIdWriteLength
	oconf.Grid = make(map[int]GridMeta)
	oconf.Filter = make(map[string]map[string]bool)
	//gfilter := map[string]map[string]bool{}
	//gridMeta := map[int]map[string]interface{}{}
	for region, dc := range cfg.Cassandra.Regions {
		for ks, gss := range getRegionConf(region, dc) {
			for _, gs := range gss {
				if _, ok := oconf.Grid[gs.Grid]; !ok {
					oconf.Grid[gs.Grid] = GridMeta{
						gs.ColWidth, gs.GridSize, gs.MaxGridsPerStack, gs.RowLength,
					}
				}
				for _, process := range gs.Processes {

					fkey := fmt.Sprintf("%s.%d.%d.%d.%d", ks, gs.Center, gs.Subcenter, gs.Grid, process)
					_, ok := oconf.Filter[fkey]
					if !ok {
						oconf.Filter[fkey] = make(map[string]bool)
					}
					oconf.Filter[fkey][strings.ToUpper(region)] = true
					//rf, _ := oconf.Filter[DataFilter{ks, gs.Center, gs.Subcenter, gs.Grid, process}]
					//if region == "us" {
					//	rf.US = true
					//} else {
					//	rf.EU = true
					//}

				}
			}
		}

	}
	// ouput yaml
	d, err := yaml.Marshal(&oconf)
	if err != nil {
		log.Fatal(err)
	}
	return d
}

func main() {

	submitConf := makeFilterConfig()
	// write config file
	//f, err := os.Create("config.yaml")
	//if err != nil {
	//	log.Fatal(err)
	//}
	//defer f.Close()
	fmt.Printf("# THIS FILE IS AUTOGENERATED. DO NOT MANUALLY EDIT THIS FILE\n%s# END", string(submitConf))
}
