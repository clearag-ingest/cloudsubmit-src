module main

go 1.15

require (
	github.com/gocql/gocql v0.0.0-20200815110948-5378c8f664e9
	github.com/kkyr/fig v0.2.0
	github.com/sirupsen/logrus v1.6.0
	gopkg.in/yaml.v2 v2.2.7
)
