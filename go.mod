module cloudingest

go 1.15

replace cloudingest/datapusher => ./internal/datapusher

replace cloudingest/rmqutils => ./internal/rmqutils

require (
	cloudingest/datapusher v0.0.0-00010101000000-000000000000
	cloudingest/rmqutils v0.0.0-00010101000000-000000000000
	github.com/google/uuid v1.1.2
	github.com/kkyr/fig v0.2.0
	github.com/sirupsen/logrus v1.6.0
	github.com/streadway/amqp v1.0.0
)
