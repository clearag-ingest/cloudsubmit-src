GOLANG_VER:=1.15.1
APP:=cloudsubmit
BUILD_DIR:=$(CURDIR)/builds
CONFIG_GEN_DIR:=$(CURDIR)/pkg/configgen
CONFIG_OUT:=$(CURDIR)/app_config.yaml

EXE_DIR:=$(BUILD_DIR)/$(APP)/lib
BIN_DIR:=$(BUILD_DIR)/$(APP)/bin
CONF_DIR:=$(BUILD_DIR)/$(APP)/conf

GOOS:=darwin  # default to osx

cloud-submit-docker:
	docker build -t cloud-submit --force-rm -f `pwd`/docker/Dockerfile .

cloud-submit: prebuild
	echo "Building cloud-submit for $(GOOS)"
	docker run --rm -v $(CURDIR):/home/dusr/code -w /home/dusr/code \
		-e CGO_ENABLED=0 -e GOOS=$(GOOS) \
		golang:$(GOLANG_VER) \
		go build -a -installsuffix cgo \
			-o builds/$(APP)/lib/cloudSubmit-$(GOOS)


cloud-submit-bsd: GOOS=freebsd
cloud-submit-bsd: cloud-submit

cloud-submit-darwin: GOOS=darwin
cloud-submit-darwin: cloud-submit

cloud-submit-linux: GOOS=linux
cloud-submit-linux: cloud-submit


build-cloud-config:
	docker build -t configgen -f $(CONFIG_GEN_DIR)/Dockerfile $(CONFIG_GEN_DIR)

gen-config:
	echo "Generating config at $(CONFIG_OUT)"
	docker run -it --rm configgen > $(CONFIG_OUT)


config: build-cloud-config gen-config

all: config
	make cloud-submit GOOS=freebsd
	make cloud-submit GOOS=linux
	make cloud-submit GOOS=darwin

clean:
	rm -rf $(BUILD_DIR)

prebuild:
	mkdir -p $(EXE_DIR)
	mkdir -p $(BIN_DIR)
	mkdir -p $(CONF_DIR)

archive:
	tar cvfz $(APP).tar.gz -C $(BUILD_DIR) $(APP)

release: clean prebuild all
	cp $(CONFIG_OUT) $(CONF_DIR)/config.yaml
	cp scripts/$(APP) $(BIN_DIR)
	chmod +x $(BIN_DIR)/$(APP)
	make archive
